#include <iostream>
#include <string>
using namespace std;

// I am adding this comment for TLLab2
void Program1()
{
    int counter = 0;

    while ( counter < 21 )
    {
        cout << counter << " ";
        counter++;
    }
}

// Here's another comment about source control
void Program2()
{
    int counter2 = 1;

    while ( counter2 < 129 )
    {
        cout << counter2 << " ";
        counter2 *= 2;
    }
}

// Source control is fun
void Program3()
{
    int secretNumber = 7;
    int playerGuess;

    do
    {
        cout << "Guess a number: ";
        cin >> playerGuess;
        if ( playerGuess > secretNumber )
        {
            cout << "Too high!" << endl;
            cout << "" << endl;
        }
        else if ( playerGuess < secretNumber )
        {
            cout << "Too low!" << endl;
            cout << "" << endl;
        }
        else
        {
            cout << "That's right!" << endl;
            cout << "" << endl;
            cout << "GAME OVER" << endl;
        }
    } while ( playerGuess != secretNumber );
}

void Program4()
{
    int userInput;

    cout << "Please enter a number between 1 and 5: ";
    cin >> userInput;
    do
    {
        cout << "Invalid entry, try again: ";
        cin >> userInput;
    } while ( 1 <= userInput && userInput >= 5);
    cout << "Thank you." << endl;
}

void Program5()
{
    float startingWage;
    float percentRaisePerYear;
    float adjustedWage;
    int yearsWorked;
    int yearCounter = 1;

    cout << "What was your starting wage? \t \t";
    cin >> startingWage;
    cout << "What % raise do you get per year? \t";
    cin >> percentRaisePerYear;
    cout << "How many years have you worked there? \t";
    cin >> yearsWorked;

    adjustedWage = startingWage;

    while ( yearCounter <= yearsWorked)
    {
        cout << "Salary at year " << yearCounter << ": \t" << adjustedWage << endl;
        adjustedWage = (adjustedWage * percentRaisePerYear / 100) + adjustedWage;
        yearCounter++;
    }
}

void Program6()
{
    int n;
    int counter3 = 1;
    int sum = 0;

    cout << "Enter a value for n: ";
    cin >> n;

    do{
        sum = sum += counter3;
        cout << "Sum: " << sum << endl;
        counter3++;
    }
    while ( counter3 <= n );
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
